import $ from 'jquery';
import 'what-input';

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used`import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

var SCROLL_OFFSET = 72;
var LOGO_MIN = 46;
var LOGO_MAX = 69;
var KOEFF = ( LOGO_MAX - LOGO_MIN ) / SCROLL_OFFSET;
var headerFixed = $('.js-header-fixed');
var headerLogo = $('.js-header-logo');

$(window).scroll(function(){
  var scroll = $(window).scrollTop();

  if (Foundation.MediaQuery.atLeast('large')) {
    if (scroll >= SCROLL_OFFSET) {
      headerFixed.addClass('with-burger')
    } else {
      headerFixed.removeClass('with-burger');
    }
  } else {
    headerFixed.removeClass('with-burger');
  }

  var logoWidth = scroll >= SCROLL_OFFSET
    ? LOGO_MIN
    : LOGO_MAX - KOEFF * scroll;

  headerLogo.width(logoWidth);
});

$(document).foundation();

$(function() {
  // $('#navigation').foundation('open');

  // Panels widget
  // ------------------------------------------------------------------------------

  $('.js-panels').each(function() {
    // var self = this;
    var navLinks = $(this).find('.js-panels-nav-item');
    var select = $(this).find('.js-panels-select');
    var selectLabel = $(this).find('.js-panels-select-label');
    var panels = $(this).find('.js-panel');

    var activeTab = ''
    var hash = window.location.hash.slice(1)

    // Ищем активный таб в url
    if (hash && $(this).find('.js-panel[data-id="' + hash + '"]').length) {
      activeTab = hash;
    // Затем в навигации
    } else if (navLinks.filter('.is-active').length) {
      activeTab = navLinks.filter('.is-active').attr('href').slice(1);
    // Затем в селекторе
    } else if (select.length) {
      activeTab = select.val();
    }

    setActiveTab(activeTab, true);

    navLinks.on('click', function(e) {
      e.preventDefault();
      setActiveTab($(this).attr('href').slice(1));
    });

    select.on('change', function(e) {
      e.preventDefault();
      setActiveTab($(this).val());
    });

    function setActiveTab(tab, isInitial) {
      if (!tab) {
        return;
      }

      const hash = '#' + tab;

      panels.removeClass('is-active');
      panels.filter('[data-id="' + tab + '"]').addClass('is-active');

      navLinks.removeClass('is-active');
      navLinks.filter('[href="' + hash + '"]').addClass('is-active');

      select.val(tab);
      selectLabel.text(select.children('option:selected').text());

      if (!isInitial) {
        window.location.hash = hash;
      }
    }
  });
});
